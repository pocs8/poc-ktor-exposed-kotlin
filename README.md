# POC - CRUD KTOR EXPOSED - KOTLIN
## Prova de conceito - CRUD KTOR EXPOSED - KOTLIN

> O projeto tem como objetivo demonstrar a ultilização do Kotlin com Ktor e Exposed, com um crud de livro.

# Tecnologias
 - Kotlin 1.3.70
 - Ktor 1.3.2
     - ktor-server-netty
     - ktor-server-core
     - ktor-gson
 - Logback 1.2.1
    - kotlin-logging (microutils) 1.6.25
 - Exposed 0.17.7
 - HikariCP 3.4.5
 - Flywaydb 6.4.1
 - H2
 - Netty (Embedded no Ktor)
 - Git
 
 # Execução
 
 A execução das aplicações são feitas através do de um comando Gradle que envoca a inicialização do Ktor.
 
 - Scripts
     - ```./gradlew run```